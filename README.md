
# hyperledger

repository for hyperledger working

* virtual-box 설정
virtual-box = https://www.virtualbox.org/wiki/Downloads
 - BIOS 에서 가상화 켜야 64bit os 설치가능
 - 클립보드 공유설정
 sudo apt-get install virtualbox-guest-dkms
 sudo apt-get install linux-headers-virtual
 클립보드 공유 옵션 - 양방향
 게스트 확장 CD 이미지 삽입
 - docker가 bionic(Ubuntu 18.04.1 LTS) 까지만 지원해서 cosmic 깔면 안됨

* VSC
VSdocs : https://code.visualstudio.com/docs?start=true
vscode-docker : docker 확장 기능

* GO 설치

GO = https://golang.org/doc/install
 - $ exprot GOPATH=$HOME/go
 - $ export PATH=$PATH:/usr/local/go/bin

* Docker 설치

Docker = https://docs.docker.com/install/linux/docker-ce/ubuntu/
	sudo groupadd docker ; \
	sudo gpasswd -a ${USER} docker && \
	sudo service docker restart
Docker Compose = https://docs.docker.com/compose/install/

* 윈도우 환경
cUrl = https://curl.haxx.se/download.html
 - cmd에서 해당 폴더로 이동후 curl 명령어로 실행가능
 - 환경변수 추가 >setx path "%PATH%;추가하고자하는경로"
%PATH%;를 하는 이유는 기존 환경변수 PATH 정보를 가져와서 기존 환경변수에 새로운 환경변수를 추가해야하기 때문입니다.
환경변수 추가(영구적) 재부팅해야 적용됨

git 설치시 Checkout Windows-style, commit Unit-style line endings 선택하지않을것!! - 가상머신 에러남

* TLS

ca 다르게 해야됨

https://stackoverflow.com/questions/45494417/enabling-tls-on-production-network-with-fabric-ca

* zookeeper

current server = 0.0.0.0으로 다르게 해야됨

https://stackoverflow.com/questions/30940981/zookeeper-error-cannot-open-channel-to-x-at-election-address

* 그 외

CORS 문제 보내는 형식을 application/x-www-form-urlencoded
https://developer.mozilla.org/ko/docs/Web/HTTP/Access_control_CORS
