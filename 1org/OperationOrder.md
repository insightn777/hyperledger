##########################################################
#
#
#    구 동 순 서
#
#
##########################################################

# docker image 다 지우기
docker image rm $(docker images --format "{{.ID}}")

# docker file 다 옮기기

#docker build -t fabricbase -f Dockerfile.base .
docker-compose up -d

cd /root/ieetu/myfabric
rm -r *

export HOSTIP=34.73.122.226
export TLS=true

###### CA #####

# CA 서버 가동	  // FABRIC_CA_SERVER_HOME에 MSP 들어감

docker exec -it ca /bin/bash

mkdir ca-tls ca-server ca-tls-admin ca-server-admin
vim ca-tls/fabric-ca-tls-config.yaml

fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove --ca.name ca0
fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove --ca.name ca0 --cafiles ../ca-tls/fabric-ca-tls-config.yaml &

# CA 관리자 등록     //FABRIC_CA_CLIENT_HOME에 MSP 들어감
fabric-ca-client enroll --caname ca0 -H /root/ieetu/myfabric/ca-server-admin -u http://admin:adminpw@$HOSTIP:7054
fabric-ca-client enroll --caname ca0-tls -H /root/ieetu/myfabric/ca-tls-admin -u http://admin:adminpw@$HOSTIP:7054

# 조직 정리
fabric-ca-client affiliation remove --force org1 -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls
fabric-ca-client affiliation remove --force org2 -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls
fabric-ca-client affiliation add Org0 -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls

fabric-ca-client affiliation remove --force org1 -H /root/ieetu/myfabric/ca-server-admin --caname ca0
fabric-ca-client affiliation remove --force org2 -H /root/ieetu/myfabric/ca-server-admin --caname ca0
fabric-ca-client affiliation add Org0 -H /root/ieetu/myfabric/ca-server-admin --caname ca0

# tls 등록
fabric-ca-client register --id.name ca0 --id.secret ca0pw --id.affiliation Org0 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls -u http://$HOSTIP:7054
fabric-ca-client register --id.name admin0 --id.secret admin0pw --id.affiliation Org0 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls -u http://$HOSTIP:7054
fabric-ca-client register --id.name peer0 --id.secret peer0pw --id.affiliation Org0 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls -u http://$HOSTIP:7054
fabric-ca-client register --id.name orderer0 --id.secret orderer0pw --id.affiliation Org0 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls -u http://$HOSTIP:7054

##fabric-ca-client register --id.name User1 --id.secret User1pw --id.affiliation Org0 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca0-tls -u http://$HOSTIP:7054

### tls enabled
fabric-ca-client enroll -u http://ca0:ca0pw@$HOSTIP:7054 -H tls --caname ca0-tls -m ca0

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

# 조직 관리자 등록    // FABRIC_CA_CLIENT_HOME에 MSP 들어감 admin=true:ecert || role=admin:ecert
fabric-ca-client register -H /root/ieetu/myfabric/ca-server-admin --caname ca0 --id.affiliation Org0 --id.name admin0 --id.secret admin0pw --id.type client --id.maxenrollments 0 --id.attrs '"hf.Registrar.Roles=client,orderer,peer,user","hf.Registrar.DelegateRoles=client,orderer,peer,user",hf.Registrar.Attributes=*,hf.GenCRL=true,hf.Revoker=true,hf.AffiliationMgr=true,hf.IntermediateCA=true,admin=true:ecert'
exit


##### ADMIN #####
docker exec -it admin /bin/bash

# 조직 관리자 MSP 생성
fabric-ca-client enroll -u http://admin0:admin0pw@$HOSTIP:7054 --caname ca0

# admincerts 폴더 생성
cp -r /root/ieetu/myfabric/msp/signcerts /root/ieetu/myfabric/msp/admincerts

# 구성원 MSP 생성
fabric-ca-client register --id.name peer0 --id.secret peer0pw --id.affiliation Org0 --id.type peer --id.maxenrollments 0 --id.attrs 'role=peer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054 --caname ca0
fabric-ca-client register --id.name orderer0 --id.secret orderer0pw --id.affiliation Org0 --id.type orderer --id.maxenrollments 0 --id.attrs 'role=orderer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054 --caname ca0

#fabric-ca-client register --id.name user0 --id.secret user0pw --id.affiliation Org0 --id.type user --id.maxenrollments 0 --id.attrs 'role=orderer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054  --caname ca0

# admin TLS enabled
mkdir tls

fabric-ca-client enroll -u http://admin0:admin0pw@$HOSTIP:7054 -H tls --caname ca0-tls -m admin0

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

# org msp 폴더 생성
mkdir -p Org0/tlscacerts
cp -r msp/admincerts Org0/
cp -r msp/cacerts Org0/
cp tls/tlsca.crt Org0/tlscacerts
exit

######## User1
fabric-ca-client register --id.name User1 --id.secret User1pw --id.affiliation Org0 --id.type user --id.maxenrollments 0 --id.attrs 'role=orderer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054 --caname ca0
mkdir -p msp/user/User1/tls

fabric-ca-client enroll -u http://User1:User1pw@$HOSTIP:7054 -H msp/user/User1 --caname ca0
fabric-ca-client enroll -u http://User1:User1pw@$HOSTIP:7054 -H msp/user/User1/tls --caname ca0-tls -m $HOSTIP
rm msp/user/User1/tls/f*
mv msp/user/User1/tls/msp/cacerts/*.pem msp/user/User1/tls/tlsca.crt
mv msp/user/User1/tls/msp/signcerts/*.pem msp/user/User1/tls/server.crt
mv msp/user/User1/tls/msp/keystore/*_sk msp/user/User1/tls/server.key
rm -r msp/user/User1/tls/msp


####### ORDERER #######
docker exec -it orderer /bin/bash

# ORDERER MSP 생성
fabric-ca-client enroll -u http://orderer0:orderer0pw@$HOSTIP:7054 --caname ca0

# TLS enabled
mkdir tls
fabric-ca-client enroll -u http://orderer0:orderer0pw@$HOSTIP:7054 -H tls --caname ca0-tls -m orderer0

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp
exit

###### PEER ######
docker exec -it peer /bin/bash

# PEER MSP 생성
fabric-ca-client enroll -u http://peer0:peer0pw@$HOSTIP:7054 --caname ca0

# TLS enabled
mkdir tls
fabric-ca-client enroll -u http://peer0:peer0pw@$HOSTIP:7054 -H tls --caname ca0-tls -m peer0

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

###### admincerts 폴더 생성 #######
cd /root/ieetu/myfabric
docker cp admin:/root/ieetu/myfabric/Org0 .

docker cp /root/ieetu/myfabric/Org0/admincerts orderer:/root/ieetu/myfabric/msp/
docker cp /root/ieetu/myfabric/Org0/admincerts peer:/root/ieetu/myfabric/msp/

# start peer
docker exec -it peer peer node start

##### ORDERER 구동 #####

###### 다른 org cert 가져옴  ($FABRIC_CA_CLIENT_HOME의 각각 admin msp 안에 admincert && cacert 필요)
#?# fabric-ca-client getcacerts   // 다른 ca 서버의 cacerts 가져옴

# gsutil cp -r Org-admin gs://fabricdocker/Org1-admin
# gsutil cp -r gs://fabricdocker/Org1-admin .

# docker cp Org1-admin orderer:/root/ieetu/myfabric/Org1-admin && docker cp Org2-admin orderer:/root/ieetu/myfabric/Org2-admin && docker cp Org0-admin orderer:/root/ieetu/myfabric/Org0-admin

# configtx.yaml 작성
docker exec -it admin bash
vim configtx.yaml

# genesisblock 생성
configtxgen -profile TaskGenesis -outputBlock genesis.block

docker cp admin:/root/ieetu/myfabric/genesis.block .
docker cp genesis.block orderer:/root/ieetu/myfabric

# orderer 실행  // 폴더에 core.yaml orderer.yaml 파일 다 있어야됨
docker exec orderer orderer

echo "34.73.122.226   orderer0 ca0 peer0 admin0" >> /etc/hosts

##### CHANNER 생성 #####

# create Channel ( admin )
configtxgen -profile TaskChannel -outputCreateChannelTx ch1.tx -channelID ch1

peer channel create -o orderer0:7050 -c ch1 -f ch1.tx --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key

# peerJoin ( admin )
peer channel join -b ch1.block

# Anchorpeer Update ( orderer >> admin )
configtxgen -profile TaskChannel -outputAnchorPeersUpdate OrgAnchor.tx -channelID ch1 -asOrg Org0

peer channel create -o orderer0:7050 -c ch1 -f OrgAnchor.tx --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key


# //  peer chaincode list -C ch1 --installed
peer chaincode install -n test -v 1.0 -p github.com/hyperledger/fabric/examples/chaincode/go/example02/cmd
peer chaincode instantiate -o orderer0:7050 -C ch1 -n test -v 1.0 -c '{"Args":["init","a","100","b","200"]}' --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key

# query
peer chaincode query -C ch1 -n test -c '{"Args":["query","a"]}'

# invoke
peer chaincode invoke -o orderer0:7050 -C ch1 -n test -c '{"Args":["invoke","a","b","20"]}'



# chainhero 옮김
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
#####go get -u github.com/hyperledger/fabric-sdk-go

docker cp chainHero admin:/root/ieetu/gopath/src/github.com/

# install chaincode ( admin ) 
peer chaincode install -n heroes-service -v 1.0 -p github.com/chainHero/heroes-service/chaincode --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key

# instantiate chaincode
peer chaincode instantiate -o orderer0:7050 -C ch1 -n heroes-service -v 1.0 -c '{"Args":["init",""]}' --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key

dep ensure

go build






###### Trouble shooting ######
##############################

# channel create error
# because it doesn't contain any IP SANs 

TLS에 IP SANs 가 안들어간단 거임 DNS SANs 가 들어감

커맨드에 -m 플래그 추가함으로써 간단히 인증서에 DNS SANs 추가 가능

DNS 사용 시 /etc/hosts 수정 필요


#==================================

# channel create error
# first record does not look like a TLS handshake

지금 통신이 TLS가 아니란 거임 커맨드에 플래그를 추가하던 환경변수에 TLS_ENABLED를 시켜주던 해야됨

#==================================

# file 수정 시
cd /root/ieetu/gopath/src/github.com/hyperledger/fabric && make peer
cd /root/ieetu/myfabric && ./createChannel.sh
vim /root/ieetu/gopath/src/github.com/hyperledger/fabric/peer/channel/create.go

###################################
