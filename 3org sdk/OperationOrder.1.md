##########################################################
#
#
#    구 동 순 서
#
#
##########################################################

# docker image 다 지우기
docker image rm $(docker images --format "{{.ID}}")

# docker file 다 옮기기

#docker build -t fabricbase -f Dockerfile.base .
export HOSTIP=35.243.169.182
export TLS=true

docker-compose up -d

cd ..
vim ca-tls/fabric-ca-tls-config.yaml
docker cp fabric-ca-tls-config.yaml ca:/root/ieetu/myfabric/ca-tls

###### CA #####

# CA 서버 가동	  // FABRIC_CA_SERVER_HOME에 MSP 들어감

docker exec -it ca /bin/bash

fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove --ca.name ca1
fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove --ca.name ca1 --cafiles ../ca-tls/fabric-ca-tls-config.yaml &

# CA 관리자 등록     //FABRIC_CA_CLIENT_HOME에 MSP 들어감
fabric-ca-client enroll --caname ca1 -H /root/ieetu/myfabric/ca-server-admin -u http://admin:adminpw@$HOSTIP:7054
fabric-ca-client enroll --caname ca1-tls -H /root/ieetu/myfabric/ca-tls-admin -u http://admin:adminpw@$HOSTIP:7054

# 조직 정리
fabric-ca-client affiliation remove --force org1 -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls
fabric-ca-client affiliation remove --force org2 -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls
fabric-ca-client affiliation add Org1 -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls

fabric-ca-client affiliation remove --force org1 -H /root/ieetu/myfabric/ca-server-admin --caname ca1
fabric-ca-client affiliation remove --force org2 -H /root/ieetu/myfabric/ca-server-admin --caname ca1
fabric-ca-client affiliation add Org1 -H /root/ieetu/myfabric/ca-server-admin --caname ca1

# tls 등록
fabric-ca-client register --id.name ca1 --id.secret ca1pw --id.affiliation Org1 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls -u http://$HOSTIP:7054
fabric-ca-client register --id.name admin1 --id.secret admin1pw --id.affiliation Org1 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls -u http://$HOSTIP:7054
fabric-ca-client register --id.name peer1 --id.secret peer1pw --id.affiliation Org1 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls -u http://$HOSTIP:7054
fabric-ca-client register --id.name orderer1 --id.secret orderer1pw --id.affiliation Org1 --id.type user -H /root/ieetu/myfabric/ca-tls-admin --caname ca1-tls -u http://$HOSTIP:7054

### tls enabled
fabric-ca-client enroll -u http://ca1:ca1pw@$HOSTIP:7054 -H tls --caname ca1-tls -m ca1

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

# 조직 관리자 등록    // FABRIC_CA_CLIENT_HOME에 MSP 들어감 admin=true:ecert || role=admin:ecert
fabric-ca-client register -H /root/ieetu/myfabric/ca-server-admin --caname ca1 --id.affiliation Org1 --id.name admin1 --id.secret admin1pw --id.type client --id.maxenrollments 0 --id.attrs '"hf.Registrar.Roles=client,orderer,peer,user","hf.Registrar.DelegateRoles=client,orderer,peer,user",hf.Registrar.Attributes=*,hf.GenCRL=true,hf.Revoker=true,hf.AffiliationMgr=true,hf.IntermediateCA=true,admin=true:ecert'
exit

##### ADMIN #####
docker exec -it admin /bin/bash

# 조직 관리자 MSP 생성
fabric-ca-client enroll -u http://admin1:admin1pw@$HOSTIP:7054 --caname ca1
mv msp/keystore/*_sk msp/keystore/serverkey.key

# admincerts 폴더 생성
cp -r /root/ieetu/myfabric/msp/signcerts /root/ieetu/myfabric/msp/admincerts


# 구성원 MSP 생성
fabric-ca-client register --id.name peer1 --id.secret peer1pw --id.affiliation Org1 --id.type peer --id.maxenrollments 0 --id.attrs 'role=peer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054 --caname ca1
fabric-ca-client register --id.name orderer1 --id.secret orderer1pw --id.affiliation Org1 --id.type orderer --id.maxenrollments 0 --id.attrs 'role=orderer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054 --caname ca1

# admin TLS enabled
mkdir tls

fabric-ca-client enroll -u http://admin1:admin1pw@$HOSTIP:7054 -H tls --caname ca1-tls -m admin1

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

# org msp 폴더 생성
mkdir -p Org1/tlscacerts
cp -r msp/admincerts Org1/
cp -r msp/cacerts Org1/
cp tls/tlsca.crt Org1/tlscacerts

# admin, peer, orderer 각각에 DNS 등
echo "35.196.70.249   orderer0 ca0 peer0 admin0" >> /etc/hosts
echo "35.243.169.182  orderer1 ca1 peer1 admin1" >> /etc/hosts
echo "35.227.56.250   orderer2 ca2 peer2 admin2" >> /etc/hosts
exit

####### ORDERER #######
docker exec -it orderer /bin/bash

# ORDERER MSP 생성
fabric-ca-client enroll -u http://orderer1:orderer1pw@$HOSTIP:7054 --caname ca1

# TLS enabled
mkdir tls
fabric-ca-client enroll -u http://orderer1:orderer1pw@$HOSTIP:7054 -H tls --caname ca1-tls -m orderer1

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

# admin, peer, orderer 각각에 DNS 등
echo "35.196.70.249   orderer0 ca0 peer0 admin0" >> /etc/hosts
echo "35.243.169.182  orderer1 ca1 peer1 admin1" >> /etc/hosts
echo "35.227.56.250   orderer2 ca2 peer2 admin2" >> /etc/hosts
exit

###### PEER ######
docker exec -it peer /bin/bash

# PEER MSP 생성
fabric-ca-client enroll -u http://peer1:peer1pw@$HOSTIP:7054 --caname ca1

# TLS enabled
mkdir tls
fabric-ca-client enroll -u http://peer1:peer1pw@$HOSTIP:7054 -H tls --caname ca1-tls -m peer1

rm tls/f*
mv tls/msp/cacerts/*.pem tls/tlsca.crt
mv tls/msp/signcerts/*.pem tls/server.crt
mv tls/msp/keystore/*_sk tls/server.key
rm -r tls/msp

# admin, peer, orderer 각각에 DNS 등
echo "35.196.70.249   orderer0 ca0 peer0 admin0" >> /etc/hosts
echo "35.243.169.182  orderer1 ca1 peer1 admin1" >> /etc/hosts
echo "35.227.56.250   orderer2 ca2 peer2 admin2" >> /etc/hosts
exit

# admincerts 폴더 생성
cd /root/ieetu/myfabric
docker cp admin:/root/ieetu/myfabric/Org1 .

docker cp /root/ieetu/myfabric/Org1/admincerts orderer:/root/ieetu/myfabric/msp/
docker cp /root/ieetu/myfabric/Org1/admincerts peer:/root/ieetu/myfabric/msp/

gsutil cp -r Org1 gs://fabricsuch/

# start peer
docker exec -it peer bash
peer node start


##### ORDERER 구동 #####

#genesis.block 가져옴 

cd /root/ieetu/myfabric/
gsutil cp gs://fabricsuch/genesis.block .
gsutil cp -r gs://fabricsuch/Org0 .
gsutil cp -r gs://fabricsuch/Org2 .

docker cp Org0 admin:/root/ieetu/myfabric/ && docker cp Org2 admin:/root/ieetu/myfabric/
docker cp Org0/tlscacerts/tlsca.crt orderer:/root/ieetu/myfabric/tls/tlsca0.crt
docker cp Org2/tlscacerts/tlsca.crt orderer:/root/ieetu/myfabric/tls/tlsca2.crt
docker cp genesis.block orderer:/root/ieetu/myfabric/

# orderer 실행  // 폴더에 configtx.yaml core.yaml orderer.yaml 파일 다 있어야됨
orderer

##### CHANNER 생성 #####

# peerJoin ( admin )
gsutil cp gs://fabricsuch/ch1.block .
docker cp ch1.block admin:/root/ieetu/myfabric

docker exec -it admin /bin/bash
peer channel join -b ch1.block

# Anchorpeer Update ( orderer0 >> admin0 )

vim configtx.yaml
configtxgen -profile TaskChannel -outputAnchorPeersUpdate Org1Anchor.tx -channelID ch1 -asOrg Org1

peer channel create -o orderer1:7050 -c ch1 -f Org1Anchor.tx --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key

##### CHAINCODE 생성 #####

# chainhero 옮김
docker cp chainHero admin:/root/ieetu/gopath/src/github.com/

mkdir /root/ieetu/gopath/bin
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# install chaincode ( admin0 ) 
peer chaincode install -n heroes-service -v 1.0 -p github.com/chainHero/heroes-service/chaincode --tls --cafile /root/ieetu/myfabric/tls/tlsca.crt --certfile /root/ieetu/myfabric/tls/server.crt --keyfile /root/ieetu/myfabric/tls/server.key

cd /root/ieetu/gopath/src/github.com/chainHero/heroes-service
dep ensure
go build

