// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 20.
//!+

// Server2 is a minimal "echo" and counter server.
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
)

func main() {
	const port = ":8000"

	http.Handle("/", http.FileServer(http.Dir("./static"))) //binary 파일이 위치한 폴더 안의 static 폴더에 있는 index.html 실행하게됨

	http.HandleFunc("/query", query)
	http.HandleFunc("/set", set)

	log.Fatal(http.ListenAndServe(port, nil)) //http 패키지 사용법 알아야되요
}

type chainValue struct {
	Key   string
	Price string
}

func set(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			log.Fatal("error") //err != nil 이건 try catch만큼 중요한 패턴
		}
	}() //defer 문 꼭 알아야되요

	// 읽어오기
	body, err1 := ioutil.ReadAll(r.Body) //r은 전달받은 http.Request 죠
	if err1 != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	var chainrequest chainValue
	err2 := json.Unmarshal(body, &chainrequest)
	if err2 != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	//값 제대로 받아왔나 체크
	if chainrequest.Key == "" {
		log.Print("null key") // 친절하게 로그 찍어주는 것이 디버그의 기본아닐까요
	} else if chainrequest.Price == "" {
		log.Print("null Price")
	}
	log.Print(chainrequest.Key + " " + chainrequest.Price)

	//셋팅
	input := `{"Args":["set","` + chainrequest.Key + `", "` + chainrequest.Price + `"]}` // `` << 이거 아시죠?? 숫자 1 왼쪽에 있는거

	log.Print(input)
	cmd1 := exec.Command("docker", "exec", "cli", "peer", "chaincode", "invoke", "-n", "mycc", "-c", input, "-C", "myc")
	// docker exec cli peer chaincode invoke -n mycc -c '{"Args":["set", "a", "20"]}' -C myc  << 기존 커맨드

	//오류 출력
	var stderr1 bytes.Buffer
	cmd1.Stderr = &stderr1
	err3 := cmd1.Run()
	if err3 != nil {
		fmt.Println(fmt.Sprint(err3) + stderr1.String())
	}
}

func query(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			log.Print("error")
		}
	}()
	//값 읽어옴
	body, err1 := ioutil.ReadAll(r.Body)
	if err1 != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	var chainrequest chainValue
	err2 := json.Unmarshal(body, &chainrequest)
	if err2 != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	//값 제대로 받아왔나 체크
	if chainrequest.Key == "" {
		log.Print("null key")
	} else if chainrequest.Price == "" {
		log.Print("null Price")
	}
	log.Print(chainrequest.Key + " " + chainrequest.Price)

	// 쿼리 진행
	input := `{"Args":["query","` + chainrequest.Key + `"]}`

	log.Print(input)
	cmd1 := exec.Command("docker", "exec", "cli", "peer", "chaincode", "query", "-n", "mycc", "-c", input, "-C", "myc")
	// docker exec cli peer chaincode query -n mycc -c '{"Args":["query","a"]}' -C myc

	out1, err3 := cmd1.Output()
	if err3 != nil {
		log.Print("output failed : %s", err3)
		panic(err3)
	}

	// 저장과 전송
	price := string(out1)
	chainresponse := chainValue{chainrequest.Key, price}
	encoder := json.NewEncoder(w)
	encoder.Encode(&chainresponse)
}

//!-

// type staticHandler struct {
// 	http.Handler
// }

// func (h *staticHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
// 	localPath := "index/" + req.URL.Path
// 	content, err := ioutil.ReadFile(localPath)
// 	if err != nil {
// 		w.WriteHeader(404)
// 		w.Write([]byte(http.StatusText(404)))
// 		return
// 	}

// 	contentType := getContentType(localPath)
// 	w.Header().Add("Content-Type", contentType)
// 	w.Write(content)
// }

// func getContentType(localPath string) string {
// 	var contentType string
// 	ext := filepath.Ext(localPath)

// 	switch ext {
// 	case ".html":
// 		contentType = "text/html"
// 	case ".css":
// 		contentType = "text/css"
// 	default:
// 		contentType = "text/plain"
// 	}
// 	return contentType
// }
