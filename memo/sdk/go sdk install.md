
* SDK = https://github.com/hyperledger/fabric-sdk-go

* Prequisit

Go 1.11
Make
Docker
Docker Compose
Git
libtool
Dep --- curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh


* install order

// dependencies
apt-get update && apt-get install -y apt-utils \
    && apt-get install -y curl python-pip git libltdl-dev tree openssh-server net-tools vim apt-transport-https software-properties-common

// Go install
curl -sL https://dl.google.com/go/go1.11.4.linux-amd64.tar.gz | tar -C /usr/local -zx && mkdir -p /root/ieetu/gopath

// env set
vim /etc/profile
export GOPATH=/root/ieetu/gopath
export GOROOT=/usr/local/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin:$GOPATH/src/github.com/hyperledger/fabric-ca/bin:$GOPATH/src/github.com/hyperledger/fabric/.build/bin
source /etc/profile

// docker install
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
&& add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
&& apt-get update && apt-get install -y docker-ce && pip install docker-compose

// hero service clone
git clone https://github.com/insightn777/fabricsdk.git

// fabric clone
mkdir -p $GOPATH/src/github.com/hyperledger && cd $GOPATH/src/github.com/hyperledger \ 
&& git clone -b release-1.4 https://github.com/hyperledger/fabric \
&& git clone -b release-1.4 https://github.com/hyperledger/fabric-ca \

// sdk install
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
go get -u github.com/hyperledger/fabric-sdk-go
cd $GOPATH/src/github.com/hyperledger/fabric-sdk-go 
make depend
make