
미진한 부분

1. 체인코드 단에서 한번 아이디로 권한검사
 >> 체인코드를 수정해야 함
 >>>> Go 언어를 배워야 함

2. 특정 조직의 앵커피어로만 접속하기 때문에 부하가 하나의 피어에 몰리는 것 
 >> chaincode 컨테이너들이 각기 다른 앵커피어랑 연결되도록
 >>>> SDK 사용하는 법을 배워야 함

3. 인스턴스 하나에 모든 네트워크 구성요소가 다 설치되는 문제
 >> 컨테이너는 따로 생성하고 SDK로 블록체인 네트워크만 구성하도록 수정해야됨야됨
 >>>> SDK 사용하는 법을 배워야 함

4. 이미 만들어진 네트워크에 체인코드를 얹어 구성하는 거
 >> 이미 만들어진 네트워크 말고 그냥 cryptogen 과 SDK로 네트워크 구성해도 무방할 듯
 >>>> SDK 사용하는 법을 배워야 함

5. 촉박한 시간으로 인해 하나의 서버만 구동하고 있음
 >> 조직별로 포트를 다르게 열어 접속 서버를 각각 두어야됨
 >>>> 4번을 해결할 시간이 필요함

6. couchDB 임계영역 문제, DB 저장 과정에서 데이터가 소실될 수 있음
 >> 도큐먼트 내에서 사용자 별로 필드를 나눠야됨 = 데이터베이스 정규화
 >>>> NoSQL 설계에 대해서 배워야 함

7 couchDB 업데이트 방식이 도큐먼트 하나를 다 가져오는데 부하가 많음
 >> 한번에 다 가져오는게 아니라 도큐먼트 분리하여 통신하는 데이터량을 줄임
 >>>> NoSQL 설계에 대해서 배워야 함


#################################################################################################

실행법

cd /root/go/src/github.com/chainHero/heroes-service/

$ make clean

cd /root/go/src/github.com/chainHero/heroes-service/fixtures

$ docker-compose up

export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$GOPATH/bin

cd /root/go/src/github.com/chainHero/heroes-service/

$ go build

./heroes-service


###############################################################################

* 편집

/heroes-service/config.yaml
/heroes-service/blockchain/setup.go

/heroes-service/blockchain/query.go
/heroes-service/blockchain/invoke.go

/heroes-service/fixtures/docker-compose.yml


/heroes-service-network/crypto-config.yaml cofigtx.yaml

###############################################################################

// Basic workflow
//
//      1) Instantiate a fabsdk instance using a configuration.
//         Note: fabsdk maintains caches so you should minimize instances of fabsdk itself.
//      2) Create a context based on a user and organization, using your fabsdk instance.
//         Note: A channel context additionally requires the channel ID.
//      3) Create a client instance using its New func, passing the context.
//         Note: you create a new client instance for each context you need.
//      4) Use the funcs provided by each client to create your solution!
//      5) Call fabsdk.Close() to release resources and caches.


################################################################################
* 소스 분석

/heroes-service/main.go : main




/heroes-service/web/app.go : 웹서버

/heroes-service/web/controllers/handler.go : 웹서버에서 쓰는 핸들러 함수들

/heroes-service/web/controllers/controller.go : 핸들러 함수에서 쓰는 구조체 선언 해놓음

/heroes-service/blockchain/setup.go : identity 만들고 channel 만들고 함 , 위의 구조체 안에 들어감


https://github.com/hyperledger/fabric/blob/b347664ed1f0d65dcc1f452ef90d2b80f273e4e7/core/chaincode/shim/interfaces.go#L31

아무래도 shim.ChaincodeStubInterface 로 함수 다 땡겨서 쓰는 것 같음

