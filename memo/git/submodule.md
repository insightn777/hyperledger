* submodule error

error : You've added another git repository inside your current repository.

하위폴더에 .git 폴더가 있으면 submodule로 인식
즉 해당폴더들은 별도의 git 저장소와 연동되어서 내 원격저장소에서 컨트롤이 안됨 

현재 폴더의 .git 폴더는 놔두고 하위의 .git 폴더 모두 지우는 법

find . -mindepth 2 -name '.git' -prune -exec rm -rf {} +

그리고 다시 add commit push 진행하면 됨


참고 : https://zetawiki.com/wiki/%ED%95%98%EC%9C%84_.git_%ED%8F%B4%EB%8D%94_%EB%AA%A8%EB%91%90_%EC%A0%9C%EA%B1%B0