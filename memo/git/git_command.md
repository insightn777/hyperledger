# git

repo 폴더 만들고 git init

git config --global user.email "your@email.com"

git config --global user.name "yourname"

git remote add (원격지 별명) (url)

git pull 원격지주소 branch || git clone 원격지주소 branch   // branch 생략하면 master 자동

git add .

git commit -m "commit message"

git push --set-upstream 원격지주소 branch

참고 : https://ljs93kr.tistory.com/26