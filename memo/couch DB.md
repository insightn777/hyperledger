
*** couchDB ***

Fauxton 접속 성공 (네트워킹 -> 방화벽 규칙 -> 포트 허용 후 접속 성공)
ip:5984/_utils

Fauxton에서 계정 생성 및 로그인, 데이터베이스, document 작성, view 생성 수행
Fauxton Visual guide :
(https://couchdb.apache.org/fauxton-visual-guide/index.html#intro)

Fauxton에서 Map & Reduce 함수의 기능 공부 및 예제 풀이 수행

데이터베이스의 기본 데이터 단위는 document 단위

replication > 복제

curl -X GET http://localhost:5984/donation-test/_design/totalMoney/_view/totalMoneyByName?key="Jung" | json_pp

(*Json 마샬링을 위해서 구조체 내 변수는 반드시 대문자로 시작!! 특정값을 쿼리할 때 \uc0”elector\uc0”사용!!)