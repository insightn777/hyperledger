

* package main

이 패키지는 라이브러리가 아닌 독립 실행형 프로그램을 정의한다.
 - package : 패키지의 역할을 정의하는 하나의 디렉토리와, 이 디렉토리 안에 있는 하나이상의 .go 소스파일로 이루어짐
 - package 정의 : 각 소스파일은 파일이 속하는 패키지를 나타내는 package 정의로 시작해야됨

go 코드 소스파일의 구성
	package main
	import packages ( 소스 파일이 import 하는 다른 패키지 목록 )
	func main() {}
	프로그램 소스코드 ( func, var, const, type )

func main() { } : main 함수가 수행하는 것이 곧 프로그램이 수행하는 것

문장이 한 줄에 두 개 이상 나오는 경우 외에는 문장이나 선언의 끝에 세미콜론 X


* package fmt
https://godoc.org/fmt

	Printf("abc%d zxc %s ... , var, var, ...)
포맷하는 방법을 지정하는 포맷 문자열

%d 10진 정수
%x %o %b 16진, 8진, 2진 정수
%f %g %e 부동소수점 수
%t 불리언
%c 룬(유니코드 문자열)
%s 문자열
%q 따옴표로 묶인 문자열 "abc"
%v 원래 형태의 값
%T 값의 타입
%% % 기호

* package bufio
https://godoc.org/bufio

입력과 출력을 효율적이고 편리하게 도와줌 ex) Scanner

	v := bufio.NewScanner(os.stdin)

	v.Scan()

입력이 토큰?으로 들어감
Scan() 나가는 법 : Ctrl + D (EOF)

	v.Text()

토큰에 들어가있는 입력을 가져올 수 있음

* package os
https://godoc.org/os

플랫폼에 독립적으로 운영체제를 제어하는 함수와 기타 값들을 제공

	os.Args
	os.Args[1:] = os.Args[1:len(os.Args)] 

문자열의 슬라이스로 커맨드라인 인수가 들어가있는 배열 변수
os.Args[0] = 명령자체의 이름
os.Args[1]~os.Args[len(os.args)] 커맨드 라인에서 실행명령어 뒤에 넣은 문자열들
len(s) : 배열 s의 원소 개수
go에서 인덱스는 첫 번째 인덱스를 포함하며, 마지막 인덱스를 제외하는 반개구간

	os.Exit()

프로세스가 종료되게 한다.

	os.Open()

열린 파일 *os.file 과 err 을 반환함

	os.file

	os.Stdin

표준 입력을 읽어들임

* package net/http
https://godoc.org/net/http

httpRequest.status = 0 오류시 폴더 경로 오류일 수 있음
웹서버폴더 index.html 파일 위치 $GOPATH/src/~/프로젝트폴더/index.html


* package flag
https://godoc.org/flag

	flag.Bool

새 불리언 타입 플래그 변수를 생성
