
* What is hugo?

https://gohugo.io/about/what-is-hugo/

휴고 (Hugo)는 캐싱을 한층 더 발전시켜 모든 HTML 파일을 컴퓨터에서 렌더링합니다. HTTP 서버를 호스팅하는 컴퓨터에 파일을 복사하기 전에 로컬에서 파일을 검토 할 수 있습니다. HTML 파일은 동적으로 생성되지 않으므로 Hugo는 정적 사이트 생성기라고 합니다.

정적 사이트 생성기 설명 :
https://blog.nacyot.com/articles/2014-01-15-static-site-generator/



Extremely fast build times (< 1 ms per page)

Integrated Disqus comment support

Integrated Google Analytics support



* Host on GitLab

https://docs.gitlab.com/ee/user/project/pages/

https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/


* mainroad theme 

https://themes.gohugo.io/mainroad/



* 순서

# hugo install

apt-get install hugo  # if you use apt-get maybe get very old version. i recommand using snap

# use hugo quickstart 

hugo new site insightn777.gitlab.io    # last parameter is directory name what your mind

# then you can download theme

cd insightn777.gitlab.io/themes/
git clone themewhatyouwant  # i recommand good theme https://themes.gohugo.io/mainroad/ and you can find other this page

cd .. 
echo 'theme = "mainroad"' >> config.toml   


# if use gitlab

vim .gitlab-ci.yml

image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master


# git

find . -mindepth 2 -name '.git' -prune -exec rm -rf {} +   # this delete themes .git So don't make submodule 관련글 링크

git init
git remote add master https://gitlab.com/insightn777/insightn777.gitlab.io.git
git pull https://gitlab.com/insightn777/insightn777.gitlab.io.git master --allow-unrelated-histories

git config --global user.email "you@example.com"
git config --global user.name "Your Name"

echo "/public" >> .gitignore

### 빈폴더 안올라가는거 게시글 링크

git add .

git commit -m "first commit"
git push --set-upstream https://gitlab.com/insightn777/insightn777.gitlab.io.git master



# if you want to add some contents
hugo new posts/postnamewhatyouwant.md

# if you want to view your site in localhost:1313


hugo server -D 

project for my archive blog https://insightn777.gitlab.io




# use gitlab

그냥 템플릿에서 고르고 레포지토리 이름만 맞춘다음에 config 파일에 url 만 수정하면 됨. 사진과 함께 정리

이대로 git pull 한 다음에 theme만 바꿔주고 설정만 바꾸면 됨

if you use gitlab. you can see pipeline-log in https://gitlab.com/<YourUsername>/<your-hugo-site>/pipelines.


discuss


Google anlaytics
