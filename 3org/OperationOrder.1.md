##########################################################
#
#
#    구 동 순 서
#
#
##########################################################

# docker image 다 지우기
docker image rm $(docker images --format "{{.ID}}")

# docker file 다 옮기기

#docker build -t fabricbase -f Dockerfile.base .
docker-compose up -d

rm -r org0-admin

export HOSTIP=10.142.0.12

###### CA #####

# CA 서버 가동	  // FABRIC_CA_SERVER_HOME에 MSP 들어감

docker exec ca fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove
docker exec -it ca /bin/bash

### tls 등록
mkdir ca-server/tls
cp ca-server/ca-cert.pem ca-server/tls/ca.crt
cp ca-server/msp/keystore/*_sk ca-server/tls/server.key

# CA 관리자 등록     //FABRIC_CA_CLIENT_HOME에 MSP 들어감
fabric-ca-client enroll -H /root/ieetu/myfabric/ca-admin -u http://admin:adminpw@$HOSTIP:7054

# 조직 정리
fabric-ca-client affiliation remove -H /root/ieetu/myfabric/ca-admin --force org1
fabric-ca-client affiliation remove -H /root/ieetu/myfabric/ca-admin --force org2
fabric-ca-client affiliation add -H /root/ieetu/myfabric/ca-admin org1

# 조직 관리자 등록    // FABRIC_CA_CLIENT_HOME에 MSP 들어감 admin=true:ecert || role=admin:ecert
fabric-ca-client register -H /root/ieetu/myfabric/ca-admin --id.affiliation org1 --id.name admin1 --id.secret admin1pw --id.type client --id.maxenrollments 0 --id.attrs '"hf.Registrar.Roles=client,orderer,peer,user","hf.Registrar.DelegateRoles=client,orderer,peer,user",hf.Registrar.Attributes=*,hf.GenCRL=true,hf.Revoker=true,hf.AffiliationMgr=true,hf.IntermediateCA=true,admin=true:ecert'
exit

##### ADMIN #####
docker exec -it admin /bin/bash

# 조직 관리자 MSP 생성
fabric-ca-client enroll -u http://admin1:admin1pw@$HOSTIP:7054

# admincerts 폴더 생성
cp -r /root/ieetu/myfabric/msp/signcerts /root/ieetu/myfabric/msp/admincerts


# 구성원 MSP 생성
fabric-ca-client register --id.name peer1 --id.secret peer1pw --id.affiliation org1 --id.type peer --id.maxenrollments 0 --id.attrs 'role=peer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054
fabric-ca-client register --id.name orderer1 --id.secret orderer1pw --id.affiliation org1 --id.type orderer --id.maxenrollments 0 --id.attrs 'role=orderer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054

# admin TLS enabled
mkdir tls
cp msp/signcerts/*.pem tls/server.crt
cp msp/cacerts/*.pem tls/ca.crt
cp msp/keystore/*_sk tls/server.key
exit

####### ORDERER #######
docker exec -it orderer /bin/bash

# ORDERER MSP 생성
fabric-ca-client enroll -u http://orderer1:orderer1pw@$HOSTIP:7054

# TLS enabled
mkdir $FABRIC_CA_CLIENT_HOME/tls
cp msp/signcerts/*.pem tls/server.crt
cp msp/cacerts/*.pem tls/ca.crt
cp msp/keystore/*_sk tls/server.key
exit

###### PEER ######
docker exec -it peer /bin/bash

# PEER MSP 생성
fabric-ca-client enroll -u http://peer1:peer1pw@$HOSTIP:7054

# TLS enabled
mkdir $FABRIC_CA_CLIENT_HOME/tls
cp msp/signcerts/*.pem tls/server.crt
cp msp/cacerts/*.pem tls/ca.crt
cp msp/keystore/*_sk tls/server.key
exit

# admincerts 폴더 생성
cd /root/ieetu/myfabric
docker cp admin:/root/ieetu/myfabric org1-admin

docker cp /root/ieetu/myfabric/org1-admin/msp/signcerts orderer:/root/ieetu/myfabric/msp/admincerts
docker cp /root/ieetu/myfabric/org1-admin/msp/signcerts peer:/root/ieetu/myfabric/msp/admincerts


##### ORDERER 구동 #####

#genesis.block 가져옴 

gsutil cp gs://fabricsuch/genesis.block .
docker cp genesis.block orderer:/root/ieetu/myfabric/

# orderer 실행  // 폴더에 configtx.yaml core.yaml orderer.yaml 파일 다 있어야됨
orderer

##### PEER 구동 #####
docker exec -it peer /bin/bash
peer node start


##### CHANNER 생성 #####

# peerJoin ( admin )
gsutil cp gs://fabricsuch/ch1.block .
docker cp ch1.block admin:/root/ieetu/myfabric

docker exec -it admin /bin/bash
peer channel join -b ch1.block

# Anchorpeer Update ( orderer0 >> admin0 )
gsutil cp gs://fabricsuch/Org1Anchor.tx .
docker cp Org1Anchor.tx admin:/root/ieetu/myfabric/

docker exec -it admin /bin/bash
peer channel create -o ${HOSTIP}:7050 -c ch1 -f Org1Anchor.tx


##### CHAINCODE 생성 #####

# install chaincode ( admin0 ) 
peer chaincode install -n testCC -v 1.0 -p github.com/hyperledger/fabric/examples/chaincode/go/example02/cmd

# //  peer chaincode list -C ch1 --installed

# query
peer chaincode query -C ch1 -n testCC -c '{"Args":["query","a"]}'

# invoke
peer chaincode invoke -o ${HOSTIP}:7050 -C ch1 -n testCC -c '{"Args":["invoke","a","b","20"]}'

