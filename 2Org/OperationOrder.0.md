##########################################################
#
#
#    구 동 순 서
#
#
##########################################################

# docker image 다 지우기
docker image rm $(docker images --format "{{.ID}}")

# docker file 다 옮기기

#docker build -t fabricbase -f Dockerfile.base .
docker-compose up -d

rm -r org0-admin

export HOSTIP=10.142.0.8

###### CA #####

# CA 서버 가동	  // FABRIC_CA_SERVER_HOME에 MSP 들어감

docker exec ca fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove
docker exec -it ca /bin/bash

### tls 등록
mkdir ca-server/tls
cp ca-server/ca-cert.pem ca-server/tls/ca.crt
cp ca-server/msp/keystore/*_sk ca-server/tls/server.key

# CA 관리자 등록     //FABRIC_CA_CLIENT_HOME에 MSP 들어감
fabric-ca-client enroll -H /root/ieetu/myfabric/ca-admin -u http://admin:adminpw@$HOSTIP:7054

# 조직 정리
fabric-ca-client affiliation remove -H /root/ieetu/myfabric/ca-admin --force org1
fabric-ca-client affiliation remove -H /root/ieetu/myfabric/ca-admin --force org2
fabric-ca-client affiliation add -H /root/ieetu/myfabric/ca-admin org0

# 조직 관리자 등록    // FABRIC_CA_CLIENT_HOME에 MSP 들어감 admin=true:ecert || role=admin:ecert
fabric-ca-client register -H /root/ieetu/myfabric/ca-admin --id.affiliation org0 --id.name admin0 --id.secret admin0pw --id.type client --id.affiliation org0 --id.maxenrollments 0 --id.attrs '"hf.Registrar.Roles=client,orderer,peer,user","hf.Registrar.DelegateRoles=client,orderer,peer,user",hf.Registrar.Attributes=*,hf.GenCRL=true,hf.Revoker=true,hf.AffiliationMgr=true,hf.IntermediateCA=true,admin=true:ecert'
exit

##### ADMIN #####
docker exec -it admin /bin/bash

# 조직 관리자 MSP 생성
fabric-ca-client enroll -u http://admin0:admin0pw@$HOSTIP:7054

# admincerts 폴더 생성
cp -r /root/ieetu/myfabric/msp/signcerts /root/ieetu/myfabric/msp/admincerts


# 구성원 MSP 생성
fabric-ca-client register --id.name peer0 --id.secret peer0pw --id.affiliation org0 --id.type peer --id.maxenrollments 0 --id.attrs 'role=peer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054
fabric-ca-client register --id.name orderer0 --id.secret orderer0pw --id.affiliation org0 --id.type orderer --id.maxenrollments 0 --id.attrs 'role=orderer:ecert' -H /root/ieetu/myfabric -u http://$HOSTIP:7054

# admin TLS enabled
mkdir tls
cp msp/signcerts/*.pem tls/server.crt
cp msp/cacerts/*.pem tls/ca.crt
cp msp/keystore/*_sk tls/server.key
exit

####### ORDERER #######
docker exec -it orderer /bin/bash

# ORDERER MSP 생성
fabric-ca-client enroll -u http://orderer0:orderer0pw@$HOSTIP:7054

# TLS enabled
mkdir $FABRIC_CA_CLIENT_HOME/tls
cp $(find $FABRIC_CA_CLIENT_HOME/msp/signcerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls/server.crt
cp $(find $FABRIC_CA_CLIENT_HOME/msp/cacerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls/ca.crt
cp $(find $FABRIC_CA_CLIENT_HOME/msp/keystore -name '*_sk') $FABRIC_CA_CLIENT_HOME/tls/server.key
exit

###### PEER ######
docker exec -it peer /bin/bash

# PEER MSP 생성
fabric-ca-client enroll -u http://peer0:peer0pw@$HOSTIP:7054

# TLS enabled
mkdir $FABRIC_CA_CLIENT_HOME/tls
cp $(find $FABRIC_CA_CLIENT_HOME/msp/signcerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls/server.crt
cp $(find $FABRIC_CA_CLIENT_HOME/msp/cacerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls/ca.crt
cp $(find $FABRIC_CA_CLIENT_HOME/msp/keystore -name '*_sk') $FABRIC_CA_CLIENT_HOME/tls/server.key
exit

# admincerts 폴더 생성
cd /root/ieetu/myfabric
docker cp admin:/root/ieetu/myfabric org0-admin

docker cp /root/ieetu/myfabric/org0-admin/msp/signcerts orderer:/root/ieetu/myfabric/msp/admincerts
docker cp /root/ieetu/myfabric/org0-admin/msp/signcerts peer:/root/ieetu/myfabric/msp/admincerts


##### ORDERER 구동 #####

###### 다른 org cert 가져옴  ($FABRIC_CA_CLIENT_HOME의 각각 admin msp 안에 admincert && cacert 필요)
#?# fabric-ca-client getcacerts   // 다른 ca 서버의 cacerts 가져옴

# gsutil cp -r org1-admin gs://fabricsuch/
# gsutil cp -r gs://fabricsuch/org1-admin .

docker cp org1-admin orderer:/root/ieetu/myfabric/org1-admin && docker cp org0-admin orderer:/root/ieetu/myfabric/org0-admin

# configtx.yaml 작성
docker exec -it orderer /bin/bash
vim configtx.yaml

# genesisblock 생성
configtxgen -profile TaskGenesis -outputBlock genesis.block

# orderer 실행  // 폴더에 genesis.block orderer.yaml 파일 다 있어야됨
orderer 


# 다른 orderer 다 시작시킴

docker cp orderer:/root/ieetu/myfabric/genesis.block .
gsutil cp genesis.block gs://fabricsuch/

##### PEER 구동 #####
docker exec -it peer /bin/bash
peer node start


##### CHANNER 생성 #####

# create Channel ( orderer0 >> admin0 )
docker exec -it orderer /bin/bash
configtxgen -profile TaskChannel -outputCreateChannelTx ch1.tx -channelID ch1

docker cp orderer:/root/ieetu/myfabric/ch1.tx .
docker cp ch1.tx admin:/root/ieetu/myfabric

docker exec -it admin /bin/bash
peer channel create -o ${HOSTIP}:7050 -c ch1 -f ch1.tx 

# peerJoin ( admin )
peer channel join -b ch1.block

# 다른 peer join
docker cp admin:/root/ieetu/myfabric/ch1.block .
gsutil cp ch1.block gs://fabricsuch/

# Anchorpeer Update ( orderer0 >> admin0 )
docker exec -it orderer /bin/bash
configtxgen -profile TaskChannel -outputAnchorPeersUpdate Org0Anchor.tx -channelID ch1 -asOrg Org0

docker cp orderer:/root/ieetu/myfabric/Org0Anchor.tx .
docker cp orderer:/root/ieetu/myfabric/Org1Anchor.tx .
docker cp Org0Anchor.tx admin:/root/ieetu/myfabric/
gsutil cp Org1Anchor.tx gs://fabricsuch/

peer channel create -o ${HOSTIP}:7050 -c ch1 -f Org0Anchor.tx

# 다른 Anchorpeer update

##### CHAINCODE 생성 #####

# install chaincode ( admin0 ) 
peer chaincode install -n testCC -v 1.0 -p github.com/hyperledger/fabric/examples/chaincode/go/example02/cmd

# //  peer chaincode list -C ch1 --installed

# instantiate chaincode
peer chaincode instantiate -o ${HOSTIP}:7050 -C ch1 -n testCC -v 1.0 -c '{"Args":["init","a","100","b","200"]}'

# query
peer chaincode query -C ch1 -n testCC -c '{"Args":["query","a"]}'

# invoke
peer chaincode invoke -o ${HOSTIP}:7050 -C ch1 -n testCC -c '{"Args":["invoke","a","b","20"]}'

###### Trouble shooting ######
##############################

# channel create error
# because it doesn't contain any IP SANs 

vim /etc/ssl/openssl.cnf

# edit

[ v3_ca ]
subjectAltName = IP : 10.142.0.13

#==================================

# channel create error
# first record does not look like a TLS handshake

일단 TLS 꺼버림

#==================================

# file 수정 시
cd /root/ieetu/gopath/src/github.com/hyperledger/fabric && make peer
cd /root/ieetu/myfabric && ./createChannel.sh
vim /root/ieetu/gopath/src/github.com/hyperledger/fabric/peer/channel/create.go

###################################