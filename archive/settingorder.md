##########################################################
#
#
#    구 동 순 서
#
#
#
##########################################################



#docker file 다 옮기기

docker build -t fabricbase -f Dockerfile.base .
docker-compose up --build


###### CA #####
docker build -t ca -f Dockerfile.ca .
docker container run -it -p 7054:7054 --name ca ca /bin/bash


# CA 서버 가동	  // FABRIC_CA_SERVER_HOME에 MSP 들어감
fabric-ca-server start -b admin:adminpw --cfg.affiliations.allowremove --cfg.identities.allowremove -d &

# CA 관리자 등록     //FABRIC_CA_CLIENT_HOME에 MSP 들어감
fabric-ca-client enroll -H /root/eetue/myfabric/ca-admin -u http://admin:adminpw@$HOSTNAME:7054

# 조직 정리
fabric-ca-client affiliation remove -H /root/eetue/myfabric/ca-admin --force org1
fabric-ca-client affiliation remove -H /root/eetue/myfabric/ca-admin --force org2
fabric-ca-client affiliation add -H /root/eetue/myfabric/ca-admin org0

# 조직 관리자 등록    // FABRIC_CA_CLIENT_HOME에 MSP 들어감 admin=true:ecert || role=admin:ecert
fabric-ca-client register -H /root/eetue/myfabric/ca-admin --id.affiliation org0 --id.name admin0 --id.secret admin0pw --id.type client --id.affiliation org0 --id.maxenrollments 0 --id.attrs '"hf.Registrar.Roles=client,orderer,peer,user","hf.Registrar.DelegateRoles=client,orderer,peer,user",hf.Registrar.Attributes=*,hf.GenCRL=true,hf.Revoker=true,hf.AffiliationMgr=true,hf.IntermediateCA=true,role=admin:ecert'

# 조직 관리자 MSP 생성
fabric-ca-client enroll -u http://admin0:admin0pw@$HOSTNAME:7054

# admincerts 폴더 생성
cp -r $FABRIC_CA_CLIENT_HOME/msp/signcerts $FABRIC_CA_CLIENT_HOME/msp/admincerts

# 구성원 MSP 생성
fabric-ca-client register -H $FABRIC_CA_CLIENT_HOME --id.name peer0 --id.secret peer0pw --id.type peer --id.affiliation org0 --id.maxenrollments 0 --id.attrs 'role=peer:ecert'
fabric-ca-client register -H $FABRIC_CA_CLIENT_HOME --id.name orderer0 --id.secret orderer0pw --id.type orderer --id.affiliation org0 --id.maxenrollments 0 --id.attrs 'role=orderer:ecert'


# CA 서버 가동
docker container start ca
docker exec ca fabric-ca-server start



####### ORDERER #######
docker build -t orderer -f Dockerfile.orderer .
docker container run -it -p 7050:7050 --name orderer orderer /bin/bash

# ORDERER MSP 생성
fabric-ca-client enroll -u http://orderer0:orderer0pw@$HOSTNAME:7054

# TLS enabled
mkdir $FABRIC_CA_CLIENT_HOME/tls
cp $(find $FABRIC_CA_CLIENT_HOME/msp/signcerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls
cp $(find $FABRIC_CA_CLIENT_HOME/msp/cacerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls
cp $(find $FABRIC_CA_CLIENT_HOME/msp/keystore -name '*_sk') $FABRIC_CA_CLIENT_HOME/tls



####### PEER #######
docker build -t peer -f Dockerfile.peer .
docker container run -it -p 7051:7051 -p 7053:7053 --name peer peer /bin/bash

# PEER MSP 생성
fabric-ca-client enroll -u http://peer0:peer0pw@$HOSTNAME:7054

# TLS enabled
mkdir $FABRIC_CA_CLIENT_HOME/tls
cp $(find $FABRIC_CA_CLIENT_HOME/msp/signcerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls/server.crt
cp $(find $FABRIC_CA_CLIENT_HOME/msp/cacerts -name '*.pem') $FABRIC_CA_CLIENT_HOME/tls/ca.crt
cp $(find $FABRIC_CA_CLIENT_HOME/msp/keystore -name '*_sk') $FABRIC_CA_CLIENT_HOME/tls/server.key

# admincerts 폴더 생성
docker cp ca:/root/eetue/myfabric/org-admin .
docker cp /root/eetue/myfabric/org0-admin/msp/signcerts orderer:/root/eetue/myfabric/msp/admincerts
docker cp /root/eetue/myfabric/org0-admin/msp/signcerts peer:/root/eetue/myfabric/msp/admincerts


#?# fabric-ca-client getcacerts   // 다른 ca 서버의 cacerts 가져옴

# 다른 org cert 가져옴
docker cp ca:/root/eetue/myfabric/org-admin .    // 컨테이너의 org-admin의 msp를 호스트로 옮김
gsutil cp -r org-admin gs://fabricdocker/org1-admin     // 호스트에서 스토리지에 업로드함
gsutil cp -r gs://fabricdocker/org1-admin . 	// 스토리지에서 호스트로 다운 받음
docker cp ./org1-admin orderer:/root/eetue/myfabric/org1-admin		//호스트에서 컨테이너로 옮김



##### ORDERER 구동 #####

# kafka/zookeeper 구동   // up 에 -d 붙이면 dettached 모드로 실행됨ㅎ

# configtx.yaml 작성

# genesisblock 생성 ($FABRIC_CA_CLIENT_HOME의 각각 admin msp 안에 admincert && cacert 필요)
configtxgen -profile TaskGenesis -outputBlock genesis.block

#?# ORDERER에서 genesisblock 생성    //genesis.block 위치 fabric-ca-client-config.yaml 과 같은 위치
docker cp ca:/root/eetue/myfabric/org-admin ./org0-admin
docker cp genesis.block orderer:/root/eetue/myfabric

# runOrderer.sh      // orderer 실행 폴더에 configtx.yaml core.yaml orderer.yaml 파일 다 있어야됨


##### PEER 구동 #####

# runPeer.sh 작성



##### CHANNER 생성 #####

# ch.tx 생성
configtxgen -profile TaskChannel -outputCreateChannelTx ch1.tx -channelID ch1         # orderer
docker cp orderer:/root/eetue/myfabric/ch1.tx .
docker cp ch1.tx peer:/root/eetue/myfabric

# createChannel.sh 작성




cd /root/eetue/gopath/src/github.com/hyperledger/fabric && make orderer

cd /root/eetue/myfabric && ./createChannel.sh

vim /root/eetue/gopath/src/github.com/hyperledger/fabric/peer/channel/create.go



vim /root/eetue/gopath/src/github.com/hyperledger/fabric/common/policies/implicitmeta.go

cd /root/eetue/myfabric && ./runOrderer.sh
