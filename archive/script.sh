## runOrderer.sh

CORE_LOGGING_GRPC=DEBUG \
ORDERER_GENERAL_LOGLEVEL=debug \
ORDERER_GENERAL_LISTENADDRESS=10.142.0.8:7050 \
ORDERER_GENERAL_GENESISMETHOD=file \
ORDERER_GENERAL_GENESISFILE=/root/eetue/myfabric/genesis.block \
ORDERER_GENERAL_LOCALMSPID=Org0MSP \
ORDERER_GENERAL_LOCALMSPDIR=/root/eetue/myfabric/msp \

ORDERER_GENERAL_TLS_ENABLED=false \
ORDERER_GENERAL_TLS_PRIVATEKEY=/root/eetue/myfabric/tls/server.key \
ORDERER_GENERAL_TLS_CERTIFICATE=/root/eetue/myfabric/tls/server.crt \
ORDERER_GENERAL_TLS_ROOTCAS=/root/eetue/myfabric/tls/ca.crt \

CONFIGTX_ORDERER_BATCHTIMEOUT=1s \
CONFIGTX_ORDERER_ORDERERTYPE=kafka \

orderer



## runPeer.sh

# CORE_PEER_CHAINCODELISTENADDRESS=fabric-peer0:7052 \   // dev 모드에서만 사용되는 env

CORE_PEER_ENDORSER_ENABLED=true \
CORE_PEER_ADDRESS=10.142.0.8:7051 \
CORE_PEER_ID=peer0 \
CORE_PEER_LOCALMSPID=org0MSP \
CORE_PEER_MSPCONFIGPATH=/root/eetue/myfabric/msp \

CORE_PEER_GOSSIP_EXTERNALENDPOINT=10.142.0.8:7051 \
CORE_PEER_GOSSIP_USELEADERELECTION=true \
CORE_PEER_GOSSIP_ORGLEADER=false \

CORE_LOGGING_GRPC=DEBUG \
CORE_LOGGING_PEER=debug \
CORE_CHAINCODE_LOGGING_LEVEL=DEBUG \

CORE_PEER_TLS_ELABLED=false \
CORE_PEER_TLS_KEY_FILE=/root/eetue/myfabric/tls/server.key \
CORE_PEER_TLS_CERT_FILE=/root/eetue/myfabric/tls/server.crt \
CORE_PEER_TLS_ROOTCERT_FILE=/root/eetue/myfabric/tls/ca.crt \
CORE_PEER_TLS_SERVERHOSTOVERRIDE=peer0 \

peer node start



## createChannel.sh ( 관리자에서 실행 )

CORE_PEER_LOCALMSPID="Org0MSP" \          
# 소속 조직의 MSP ID
CORE_PEER_MSPCONFIGPATH=/root/eetue/myfabric/msp \       
# MSP 폴더 경로 ex) /root/eetue/myfabric/msp
CORE_PEER_ADDRESS=10.142.0.8:7051 \      
# hostsname:7051

CORE_PEER_TLS_ROOTCERT_FILE=/root/eetue/myfabric/tls/ <tls폴더 cacert파일 경로> \

peer channel create -o 10.142.0.8:7050 -c ch1 -f ch1.tx


## peerJoin.sh ( 관리자에서 실행 )

CORE_PEER_LOCALMSPID="org0MSP" \
CORE_PEER_MSPCONFIGPATH=/root/eetue/myfabric/msp \
CORE_PEER_ADDRESS=10.142.0.3:7051 \
CORE_PEER_TLS_ROOTCERT_FILE=/root/eetue/myfabric/tls \

FABRIC_LOGGING_SPEC=debug \
GRPC_GO_LOG_SEVERITY_LEVEL=info \
GRPC_GO_LOG_VERBOSITY_LEVEL=2 \

peer channel create -o 10.142.0.3:7050 -c ch1 -f ch1.tx



## anchor.sh ( 관리자에서 실행 )

export CORE_PEER_LOCALMSPID="<소속 MSP ID>"
export CORE_PEER_MSPCONFIGPATH=<MSP폴더 경로>
export CORE_PEER_ADDRESS=peer2:7051

peer channel create -o orderer0:7050 -c ch1 -f Org



## installCC.sh ( 관리자에서 실행 )

export CORE_PEER_LOCALMSPID="<소속 MSP ID>"
export CORE_PEER_MSPCONFIGPATH=<MSP폴더 경로>
export CORE_PEER_ADDRESS=peer2:7051
